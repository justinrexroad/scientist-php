<?php namespace Scientist;

class Observation {
    public $name;
    public $experiment;
    public $now;
    public $value;
    public $exception;
    public $duration;

    function __construct($name, \Scientist\Experiment &$experiment, \Closure $callback) {
        $this->name = $name;
        $this->experiment =& $experiment;
        $this->now = microtime(true);

        try {
            $this->value = $callback();
        } catch (\Exception $e) {
            $this->exception = $e;
        }

        $this->duration = microtime(true) - $this->now;
    }


    /* not 100% sure if this is needed, in ruby this is so that a observation.eql? is comparable */
    public function hash() {
        return ;
    }

    public function cleaned_value() {
        if ($this->value) {
            return $this->experiment->clean_value($this->value);
        }
    }

    public function raised() {
        return  !(is_null($this->exception));
    }

    public function equivalent_to(\Scientist\Observation $other, \Closure $comparator = NULL) {
        if (!is_a($other, '\Scientist\Observation')) return false;

        $values_are_equal = false;
        $both_raised = $other->raised() && $this->raised();
        $neither_raised = !$other->raised() && !$this->raised();

        if ($neither_raised) {
            if ($comparator) {
                $values_are_equal = $comparator($this->value, $other->value);
            } else {
                $values_are_equal = $this->value == $other->value;
            }
        }

        $exceptions_are_equivalent = $both_raised
            && get_class($other->exception) == get_class($this->exception)
            && $other->exception->getMessage() == $this->exception->getMessage();
        return ($neither_raised && $values_are_equal) || ($both_raised && $exceptions_are_equivalent);
    }
}
