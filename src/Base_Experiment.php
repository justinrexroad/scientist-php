<?php namespace Scientist;

class Base_Experiment extends Experiment {
    public $result;

    public function __construct($name) {
        $this->name = $name;
    }

    public function publish($result) {
        $this->result = $result;
    }
}