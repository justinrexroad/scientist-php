<?php namespace Scientist;

class Experiment {

    private $_scientist_before_run;
    private $_scientist_behaviors;
    private $_scientist_cleaner;
    private $_scientist_comparator;
    private $_scientist_run_if;
    public $name = 'Experiment';

    /* Helper method to generate a simple experiment */
    static function forge($name) {
        return new \Scientist\Base_Experiment($name);
    }

    public function before_run($closure) {
        $this ->_scientist_before_run = $closure;
    }

    public  function behaviors() {
        if (!$this->_scientist_behaviors) {
            $this->_scientist_behaviors = new \stdClass();
        }
        return $this->_scientist_behaviors;
    }

    public function clean($closure) {
        $this->_scientist_cleaner = $closure;
    }

    public function clean_value($value) {
        try {
            if ($this->_scientist_cleaner) {
                $this->_scientist_cleaner($value);
            } else {
                return $value;
            }
        } catch (\Exception $e) {
            $this->raised('clean', $e);
            return $value;
        }
    }

    public function compare(\Closure $closure){
        $this->_scientist_comparator = $closure;
    }

    /* science.use */
    public function control(\Closure $closure) {
        $this->candidate($closure, 'control');
    }

    /* science.try */
    public function candidate(\Closure $closure, $name = NULL) {
        if(!$name) {
            $name = 'candidate';
        }

        if(property_exists($this->behaviors(), $name)){
            throw new \Scientist\Exceptions\BehaviorNotUnique();
        }
        $this->behaviors()->$name = $closure;
    }

    public function raised($operation, $error) {
        throw $error;
    }

    public function run($name = NULL) {
        if (!$name) {
            $name = 'control';
        }

        if (!property_exists($this->behaviors(),$name)) {
            throw new \Scientist\Exceptions\BehaviorMissing();
        }

        if (!$this->should_experiment_run()) return $this->behaviors()->$name->__invoke();

        if ($this->_scientist_before_run) {
            $this->_scientist_before_run();
        }

        $observations = array();

        $behaviors = $this->behaviors();

        foreach ($behaviors as $key => $value) {
            $observations[$key] = new \Scientist\Observation($key, $this, $value);
        }

        //  THAR BE DRAGONS
        //  ___________________________________________________
        //  @@@@@@@@@@@@@@@@@@@@@**^^""~~~"^@@^*@*@@**@@@@@@@@@
        //  @@@@@@@@@@@@@*^^'"~   , - ' '; ,@@b. '  -e@@@@@@@@@
        //  @@@@@@@@*^"~      . '     . ' ,@@@@(  e@*@@@@@@@@@@
        //  @@@@@^~         .       .   ' @@@@@@, ~^@@@@@@@@@@@
        //  @@@~ ,e**@@*e,  ,e**e, .    ' '@@@@@@e,  "*@@@@@'^@
        //  @',e@@@@@@@@@@ e@@@@@@       ' '*@@@@@@    @@@'   0
        //  @@@@@@@@@@@@@@@@@@@@@',e,     ;  ~^*^'    ;^~   ' 0
        //  @@@@@@@@@@@@@@@^""^@@e@@@   .'           ,'   .'  @
        //  @@@@@@@@@@@@@@'    '@@@@@ '         ,  ,e'  .    ;@
        //  @@@@@@@@@@@@@' ,&&,  ^@*'     ,  .  i^"@e, ,e@e  @@
        //  @@@@@@@@@@@@' ,@@@@,          ;  ,& !,,@@@e@@@@ e@@
        //  @@@@@,~*@@*' ,@@@@@@e,   ',   e^~^@,   ~'@@@@@@,@@@
        //  @@@@@@, ~" ,e@@@@@@@@@*e*@*  ,@e  @@""@e,,@@@@@@@@@
        //  @@@@@@@@ee@@@@@@@@@@@@@@@" ,e@' ,e@' e@@@@@@@@@@@@@
        //  @@@@@@@@@@@@@@@@@@@@@@@@" ,@" ,e@@e,,@@@@@@@@@@@@@@
        //  @@@@@@@@@@@@@@@@@@@@@@@~ ,@@@,,0@@@@@@@@@@@@@@@@@@@
        //  @@@@@@@@@@@@@@@@@@@@@@@@,,@@@@@@@@@@@@@@@@@@@@@@@@@
        //  """""""""""""""""""""""""""""""""""""""""""""""""""

        $control = $observations[$name];

        $result = new \Scientist\Result($this, $observations, $control);


        try {
            $this->publish($result);
        } catch (\Exception $e) {
            $this->raised('publish', $e);
        }


        if ($control->raised()) {
            throw $control->exception;
        }

        return $control->value;
    }

    public function run_if(\Closure $closure) {
        $this->_scientist_run_if = $closure;
    }

    public function run_if_block_allows() {
        try {
            return ($this->_scientist_run_if ? $this->_scientist_run_if() : true);
        } catch (\Exception $e) {
            $this->raised('run_if', $e);
            return false;
        }
    }

    public function should_experiment_run() {
        try {
            return (count((array)$this->behaviors()) > 1) && $this->enabled() && $this->run_if_block_allows();
        } catch (\Exception $e) {
            $this->raised('enabled', $e);
            return false;
        }
    }

    public function observations_are_equivalent(\Scientist\Observation $control, \Scientist\Observation  $candidate) {
        if ($this->_scientist_comparator){
            return $control->equivalent_to($candidate, $this->_scientist_compator);
        } else {
            return $control->equivalent_to($candidate);
        }
    }

    public function enabled() {
        return true;
    }
}
