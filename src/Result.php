<?php namespace Scientist;

class Result {
    public $candidates;
    public $control;
    public $experiment;
    public $ignored;
    public $mismatched;
    public $observations;

    function __construct(\Scientist\Experiment &$experiment, $observations = array(), \Scientist\Observation $control = NULL ) {
        $this->experiment =& $experiment;
        $this->observations = $observations;
        $this->control = $control;
        $this->candidates = $observations;
        // Remove control from candidates
        unset($this->candidates[$control->name]);

        $this->evaluate_candidates();
    }

    public function context() {
        return $this->experiment->context;
    }

    public function experiment_name() {
        return $this->experiment->name;
    }

    public function all_matched() {
        return (count($this->mismatched) == 0) && !$this->ignored;
    }

    public function has_mismatches() {
        return (count($this->mismatched) > 0);
    }

    public function has_ignored_mismatches() {
        return (count($this->ignored) > 0);
    }

    private function evaluate_candidates() {
        $mismatched = array_filter($this->candidates, function($candidate){
            return !($this->experiment->observations_are_equivalent($this->control, $candidate));
        });

//        $this->ignored = array_filter($mismatched, function($candidate) {
//            return $this->experiment->ignore_mismatched_observation($this->control, $candidate);
//        });

        $this->mismatched = $mismatched;
    }
}
